<?php


class RandomSumControllerTest extends TestCase{

    public function setUp(){
        parent::setUp();
        $this->mockInterface = Mockery::mock("RandomSumInterface");
        $this->app->instance(RandomSumInterface::class,$this->mockInterface);
    }

    public function testPing(){

        $this->mockInterface->shouldReceive("randomInt")
        ->andReturn(0);        

        $this->client->request("GET","/ping");
        $this->assertTrue($this->client->getResponse()->isOk());

        $data = $this->client
        ->getResponse()
        ->getOriginalContent();

        $status = $data["status"];
 
        $this->assertEquals(200,$status);

    }


    public function testZero(){
        
        $this->mockInterface->shouldReceive("randomInt")
                       ->andReturn(0);

      

        $this->client->request("POST","/randsum",["number"=>0]);
        $this->assertTrue($this->client->getResponse()->isOk());

        $data = $this->client->getResponse()->getOriginalContent();
        $answer = $data["answer"];
        $this->assertEquals("zero", $answer);

    }

    public function testPositive(){
        
        $this->mockInterface->shouldReceive("randomInt")
                              ->andReturn(10);


        $this->client->request("POST","/randsum",["number"=>0]);
        $this->assertTrue($this->client->getResponse()->isOk());

        $data = $this->client->getResponse()->getOriginalContent();
        $answer = $data["answer"];
        $this->assertEquals("positive", $answer);

    }

    public function testNegative(){
        
        $this->mockInterface->shouldReceive("randomInt")
                       ->andReturn(-1);

        $this->client->request("POST","/randsum",["number"=>0]);
        $this->assertTrue($this->client->getResponse()->isOk());

        $data = $this->client->getResponse()->getOriginalContent();
        $answer = $data["answer"];
        $this->assertEquals("negative", $answer);

    }

    public function tearDown(){
        Mockery::close();
    }


}
<?php

class RandomSumController extends \BaseController {

	public function __construct(RandomSumInterface $randomSumInterface){
		$this->randomSumInterface = $randomSumInterface;
	}

	public function ping(){
		return ["response" => "ok", 
				"status" => 200,
				"random" => $this->randomSumInterface->randomInt()];
	}


	public function randSum(){

		$number = Input::get("number");
		$randNum = $this->randomSumInterface->randomInt();
		$total = $number + $randNum;
		$answer = "";
		
		if($total == 0){
			$answer = "zero";
		} else if($total > 1){
			$answer = "positive";
		} else{
			$answer = "negative";
		}

		return [
			"answer" => $answer, 
			"randomNumber" => $randNum, 
			"num" => $number, 
			"total"=>$total
		];

	}

}

<?php

interface RandomSumInterface{

    public function randomInt();
    public function limits();

}

class RandomSumImpl implements RandomSumInterface{

    public function __construct($start,$end){
        $this->start = $start;
        $this->end = $end;
    }

    public function randomInt(){
        return rand($this->start,$this->end);
    }

    public function limits(){
        return ["start"=>$this->start,
                "end"=>$this->end
                ];
    }

}